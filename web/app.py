from flask import Flask, jsonify, request
from flask_restful import Api, Resource


app = Flask(__name__)
api = Api(app)

@app.route("/")
def index():
    return "Hello World!"

class Sum(Resource): 

    def sum(self, x, y):
        raise NotImplementedError

    def post(self):
        try:
            data = request.get_json()
            ret = self.sum(data["x"],data["y"])
            status = 200
        except ZeroDivisionError as e:
            ret = "y is zero"
            status = 418
        except Exception as e:
            ret = repr(e) # TODO we're exposing part of the traceback here and should handle validation better
            status = 400

        resp = {"result": ret, "status": status}
        return resp, status

class Add(Sum):

    def sum(self, x, y):
        return x + y

class Subtract(Sum):

    def sum(self, x, y):
        return x - y

class Multiply(Sum):
    
    def sum(self, x, y):
        return x * y

class Divide(Sum):

    def sum(self, x, y):
        return x / y
   
api.add_resource(Add, "/add")
api.add_resource(Subtract, "/subtract")
api.add_resource(Multiply, "/multiply")
api.add_resource(Divide, "/divide")

if __name__ == "__main__":
    app.run(host="0.0.0.0")